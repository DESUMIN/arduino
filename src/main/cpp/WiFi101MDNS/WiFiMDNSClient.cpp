/*
 * Copyright (C) 2018 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the MDNS Client package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */

#include "WiFiMDNSClient.h"

WiFiMDNSClient::WiFiMDNSClient() {
  hostname = NULL;
  writeerror = false;
}

WiFiMDNSClient::~WiFiMDNSClient() {
}

bool WiFiMDNSClient::begin(const char* hostname) {
  if (inRead()) return false;
  //
  if (!beginRead(hostname)) return false;
  //
  beginWrite();
  writeShort(0); // transaction id
  writeShort(0); // flags
  writeShort(1); // number of questions
  writeShort(0); // number of answers
  writeShort(0); // number of authority resource records
  writeShort(0); // number of additional resource records
  writeQuestion(hostname);
  endWrite();
  //
  if (getWriteError()) {
    clearWriteError();
    //
    return false;
  }
  //
  return true;
}

bool WiFiMDNSClient::poll(IPAddress& answer) {
  if (!inRead()) return false;
  //
  if (receivePacket() >= 12) {
    uint16_t transactionId = readShort();
    uint16_t flags = readShort();
    uint16_t numberOfQuestions = readShort();
    uint16_t numberOfAnswers = readShort();
    uint16_t numberOfAuthorityResourceRecords = readShort();
    uint16_t numberOfAdditionalResourceRecords = readShort();
    //
    if (numberOfQuestions == 0 && numberOfAnswers >= 1) {
      if (matchResourceRecord(answer)) {
        endRead();
        //
        return true;
      }
    }
  }
  //
  return false;
}

void WiFiMDNSClient::beginWrite() {
  if (!udp.beginPacket(IPAddress(224, 0, 0, 251), 5353)) writeerror = true;
}

void WiFiMDNSClient::writeQuestion(const char* hostname) {
  writeQName(hostname);
  writeShort(1); // qtype A record
  writeShort(1); // qclass
}

void WiFiMDNSClient::writeQName(const char* qname) {
  uint8_t labellength = 0;
  const char* label = qname;
  //
  while (*qname) {
    if (*qname == '.') {
      writeLengthLabel(labellength, label);
      labellength = 0;
      label = qname + 1;
    } else {
      labellength++;
    }
    //
    qname++;
  }
  //
  writeLengthLabel(labellength, label);
  writeByte(0); // terminator
}

void WiFiMDNSClient::writeLengthLabel(uint8_t length, const char* label) {
  writeByte(length);
  //
  if (writeerror) return;
  //
  if (udp.write(label, length) != length) writeerror = true;
}

void WiFiMDNSClient::writeShort(uint16_t value) {
  writeByte(value >> 8);
  writeByte(value & 255);
}

void WiFiMDNSClient::writeByte(uint8_t value) {
  if (writeerror) return;
  //
  if (!udp.write(value)) writeerror = true;
}

void WiFiMDNSClient::endWrite() {
  if (!udp.endPacket()) writeerror = true;
}

bool WiFiMDNSClient::getWriteError() {
  return writeerror;
}

void WiFiMDNSClient::clearWriteError() {
  writeerror = false;
}

bool WiFiMDNSClient::beginRead(const char* _hostname) {
  if (udp.beginMulticast(IPAddress(224, 0, 0, 251), 5353)) {
    hostname = _hostname;
    //
    return true;
  }
  //
  return false;
}

bool WiFiMDNSClient::inRead() {
  return hostname != NULL;
}

int WiFiMDNSClient::receivePacket() {
  return udp.parsePacket();
}

bool WiFiMDNSClient::matchResourceRecord(IPAddress& answer) {
  if (!matchQName(hostname)) return false;
  //
  uint16_t typecode = readShort();
  uint16_t classcode = readShort() & 0x7fff;
  int ttl = readInt();
  int length = readShort();
  //
  if (typecode != 1 || classcode != 1 || ttl == 0 || length != 4) return false;
  //
  answer = IPAddress(readByte(), readByte(), readByte(), readByte());
  //
  return true;
}

bool WiFiMDNSClient::matchQName(const char* qname) {
  uint8_t labellength = readByte();
  //
  while (*qname) {
    if (*qname == '.') {
      if (labellength) return false;
      //
      labellength = readByte();
    } else {
      char c = (char) readByte();
      //
      if (c >= 'A' && c <= 'Z') c += ('a' - 'A'); // to lower case
      //
      if (!labellength || *qname != c) return false;
      //
      labellength--;
    }
    //
    qname++;
  }
  //
  if (labellength) return false; // remaining label length has to be 0
  //
  return !readByte(); // terminator
}

uint32_t WiFiMDNSClient::readInt() {
  uint32_t value = readShort();
  value <<= 16;
  value += readShort();
  //
  return value;
}

uint16_t WiFiMDNSClient::readShort() {
  uint16_t value = readByte();
  value <<= 8;
  value += readByte();
  //
  return value;
}

uint8_t WiFiMDNSClient::readByte() {
  return udp.read();
}

void WiFiMDNSClient::endRead() {
  hostname = NULL;
  udp.stop();
}

bool WiFiMDNSClient::inLocalDomain(const char* hostname) {
  int len = strlen(hostname);
  //
  if (len < 6) return false;
  //
  hostname += (len - 6);
  //
  return !strcmp(hostname, ".local");
}
