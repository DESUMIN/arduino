/*
 * Copyright (C) 2018 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the MDNS Client package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */

#ifndef WIFIMDNSCLIENT_H
#define WIFIMDNSCLIENT_H

#include <Arduino.h>
#include "WiFiUdp.h"

class WiFiMDNSClient {
  private:
    WiFiUDP udp;
    const char* hostname;
    bool writeerror;

    void beginWrite();
    void writeQuestion(const char* hostname);
    void writeQName(const char* qname);
    void writeLengthLabel(uint8_t length, const char* label);
    void writeShort(uint16_t value);
    void writeByte(uint8_t value);
    void endWrite();
    bool getWriteError();
    void clearWriteError();

    bool beginRead(const char* hostname);
    bool inRead();
    int receivePacket();
    bool matchResourceRecord(IPAddress& answer);
    bool matchQName(const char* qname);
    uint32_t readInt();
    uint16_t readShort();
    uint8_t readByte();
    void endRead();

  public:
    WiFiMDNSClient();
    virtual ~WiFiMDNSClient();
    bool begin(const char* hostname);
    bool poll(IPAddress& answer);
    static bool inLocalDomain(const char* hostname);
};

#endif
