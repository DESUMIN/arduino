## MQTT Client

MQTT Client ist eine Bibliothek, mit der Zeichenketten an ein Topic eines MQTT
Brokers publiziert werden können. Die Bibliothek verwendet die Bibliothek Cooperative
Multitasking (siehe unten).

Um eine Instanz der Klasse `MQTTClient` zu erzeugen, benötigt man eine Referenz auf eine
Instanz der Klasse `CooperativeMultitasking`, eine Referenz auf eine Netzwerkverbindung
(z.B. aus `WiFi101`), den Hostnamen des MQTT Brokers, dessen Portnummer (gewöhnlich 1883)
und eine Client Id. Wenn der Broker das erwartet, dann müssen auch ein Benutzername und
ein Passwort angegeben werden.

Einer neuen Instanz der Klasse `MQTTTopic` müssen eine Referenz auf den `MQTTClient`
und der Name des Topics übergeben werden.

    #include <Client.h>
    #include <WiFi101.h>
    #include <CooperativeMultitasking.h>
    #include <MQTTClient.h>

    char ssid[] = "...";
    char pass[] = "...";
    char host[] = "broker.hivemq.com";
    char clientid[] = "...";
    char username[] = "...";
    char password[] = "...";
    char topicname[] = "amotzek/hello";

    CooperativeMultitasking tasks;
    WiFiClient wificlient;
    MQTTClient mqttclient(&tasks, &wificlient, host, 1883, clientid, username, password);
    MQTTTopic topic(&mqttclient, topicname);

Die Methode `connect()` von `MQTTClient` stellt eine Verbindung zum Broker her. Eine
Zeichenkette kann mit der Methode `publish("...")` des `MQTTTopic` publiziert werden. Beide
Methoden arbeiten asynchron. Sie erzeugen Aufgaben, die von der Instanz von
`CooperativeMultitasking` verwaltet werden. Deshalb muss man deren Methode `run()` aufrufen,
bis alle Aufgaben abgearbeitet sind.

    void setup() {
      Serial.begin(9600);
      //
      while (!Serial) {
        delay(1000);
      }
      //
      WiFi.begin(ssid, pass);
      delay(10000);
    }

    void loop() {
      if (mqttclient.connect()) {
        topic.publish("Hello");
        //
        while (tasks.available()) {
          tasks.run();
        }
        //
        mqttclient.disconnect();
      }
      //
      switch (WiFi.status()) {
        case WL_CONNECT_FAILED:
        case WL_CONNECTION_LOST:
        case WL_DISCONNECTED: WiFi.begin(ssid, pass);
      }
      //
      delay(30000);
    }


## Cooperative Multitasking

Cooperative Multitasking ist eine Bibliothek, mit der mehrere Funktionen (fast)
gleichzeitig oder unabhängig voneinander ausgeführt werden können.

Um das zu ermöglichen, verwalten Instanzen der Klasse `CooperativeMultitasking`
Listen von Aufgaben. Jeder Sketch braucht nur eine Instanz der Klasse.

    #include <CooperativeMultitasking.h>

    CooperativeMultitasking tasks;

Funktionen, die im Rahmen einer Aufgabe ausgeführt werden sollen, müssen eine
sogenannte Forward Declaration haben:

    Continuation f;

    void f() {
      // Code hier
      // hier können und sollen auch now, after, ifThen, ifForThen
      // und onlyOneOf verwendet werden, jedoch nicht delay!
    }

Außerdem kann es noch Funktionen geben, die für das Testen von Bedingungen
verwendet werden. Diese müssen eine andere Forward Declaration haben:

    Guard g;

    bool g() {
      // Code hier
      return true; // oder return false;
    }

Aufgaben können mit `now`, `after`, `ifThen` und `ifForThen` in die Liste
aufgenommen werden:

* Mit `tasks.now(f);` wird der Aufruf der Funktion `f` an den Anfang der Liste
  gestellt. Der Aufruf von `f` findet dann sofort statt.
* Mit `after(1000, f);` findet der Aufruf der Funktion `f` erst in 1000
  Millisekunden statt.
* Mit `ifThen(g, f);` findet der Aufruf der Funktion `f` statt, sobald die
  Funktion `g` den Wert true liefert.
* Mit `ifForThen(g, 300, f);` findet der Aufruf der Funktion `f` statt,
  wenn `g` mindestens 300 Millisekunden lang den Wert `true` geliefert hat.

Mit der Methode `onlyOneOf` kann man deklarieren, dass nur eine von mehreren
Aufgaben ausgeführt werden soll - die, die als erste aktiv wird. Ein Beispiel
dafür findet sich in [BlinkWhenItsDark.ino](https://bitbucket.org/amotzek/arduino/src/0f19b22f4a793a0e066c1278d922b952261e6ed8/src/main/cpp/CooperativeMultitasking/examples/BlinkWhenItsDark/?at=master).

In der Funktion `loop()` muss die Methode `run()` aufgerufen werden. So werden die
Aufgaben ausgeführt, sobald sie an der Reihe sind.

    void loop() {
      tasks.run();
    }

Damit beim Start des Sketch überhaupt eine Aufgabe zur Ausführung kommt,
muss man diese im Setup in die Liste aufnehmen.

    void setup() {
      // anderer Code hier
      tasks.now(f);
    }
